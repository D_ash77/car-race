package com.game.carrace.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.game.carrace.CarRace;

/**
 * DesktopLauncher.java - launcher class for app
 * @author Mateusz Kud&#x142;a
 */
public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new CarRace(), config);
	}
}
