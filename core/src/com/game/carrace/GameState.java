package com.game.carrace;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

import java.io.Serializable;

/**
 * GameState.java - class for holding models position, rotation and other game info
 * @author Mateusz Kud&#x142;a
 */
public class GameState implements Serializable{
    private Vector3 carPosition;
    private Quaternion carRotation;
    private int checkpointCounter;
    private boolean checkpointState[];
    private float timeCounter;
    private Vector3 camPosition;


    public Vector3 getCamPosition() {
        return camPosition;
    }

    public void setCamPosition(Vector3 camPosition) {
        this.camPosition = camPosition;
    }

    public Vector3 getCarPosition() {
        return carPosition;
    }

    public void setCarPosition(Vector3 carPosition) {
        this.carPosition = carPosition;
    }

    public Quaternion getCarRotation() {
        return carRotation;
    }

    public void setCarRotation(Quaternion carRotation) {
        this.carRotation = carRotation;
    }

    public int getCheckpointCounter() {
        return checkpointCounter;
    }

    public void setCheckpointCounter(int checkpointCounter) {
        this.checkpointCounter = checkpointCounter;
    }

    public boolean[] getCheckpointState() {
        return checkpointState;
    }

    public void setCheckpointState(boolean[] checkpointState) {
        this.checkpointState = checkpointState;
    }

    public float getTimeCounter() {
        return timeCounter;
    }

    public void setTimeCounter(float timeCounter) {
        this.timeCounter = timeCounter;
    }
}
