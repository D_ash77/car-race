package com.game.carrace;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.game.carrace.screens.GameScreen;
import com.game.carrace.screens.MainMenuScreen;

/**
 * CarRace.java - main application class
 * @author Mateusz Kud&#x142;a
 */
public class CarRace extends Game {

	/**
	 * Loads main menu
	 */
	@Override
	public void create () {
		setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render () {
        super.render();
	}
	
	@Override
	public void dispose () {

	}
}
