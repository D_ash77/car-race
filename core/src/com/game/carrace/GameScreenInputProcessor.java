package com.game.carrace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.game.carrace.screens.GameScreen;


/**
 * GameScreenInputProcessor.java - class for handling keyboard
 * @author Mateusz Kud&#x142;a
 */
public class GameScreenInputProcessor implements InputProcessor {
    GameScreen gameScreen;

    public GameScreenInputProcessor(GameScreen gameScreen){
        this.gameScreen = gameScreen;
    }

    /**
     * Enables move depending on pressed key
     * @param keycode pressed key
     * @return false
     */
    @Override
    public boolean keyDown(int keycode) {
        switch (keycode){
            case Input.Keys.UP:
                gameScreen.setForwardMove(true);
                break;
            case Input.Keys.DOWN:
                gameScreen.setBackwardMove(true);
                break;
            case Input.Keys.LEFT:
                gameScreen.setLeftMove(true);
                break;
            case Input.Keys.RIGHT:
                gameScreen.setRightMove(true);
                break;
        }
        return false;
    }

    /**
     * Disables move depending on released key
     * Escape saves game state end exits the application
     * @param keycode released key
     * @return false
     */
    @Override
    public boolean keyUp(int keycode) {
        switch (keycode){
            case Input.Keys.UP:
                gameScreen.setForwardMove(false);
                break;
            case Input.Keys.DOWN:
                gameScreen.setBackwardMove(false);
                break;
            case Input.Keys.LEFT:
                gameScreen.setLeftMove(false);
                break;
            case Input.Keys.RIGHT:
                gameScreen.setRightMove(false);
                break;
            case Input.Keys.ESCAPE:
                gameScreen.saveGameState();
                Gdx.app.exit();
                break;
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
