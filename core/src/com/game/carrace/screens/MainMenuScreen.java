package com.game.carrace.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.game.carrace.CarRace;

/**
 * MainMenuScreen.java - class for displaying main menu
 * @author Mateusz Kud&#x142;a
 */
public class MainMenuScreen implements Screen {
    private CarRace game;

    /**
     * Stage holds 2d elements
     */
    Stage stage;
    SpriteBatch batch;
    Texture texture;
    ImageButton startButton;
    Texture startButtonTexture;
    TextureRegion starButtonTexRegion;
    TextureRegionDrawable startButtonTexRegionDrawable;

    ImageButton exitButton;
    Texture exitButtonTexture;
    TextureRegion exitButtonTexRegion;
    TextureRegionDrawable exitButtonTexRegionDrawable;

    ImageButton loadButton;
    Texture loadButtonTexture;
    TextureRegion loadButtonTexRegion;
    TextureRegionDrawable loadButtonTexRegionDrawable;

    public MainMenuScreen(CarRace game){
        this.game = game;
        batch = new SpriteBatch();
        stage = new Stage(new ScreenViewport());

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void show() {
        /**
         * Create buttons from file
         */
        startButtonTexture = new Texture(Gdx.files.internal("startbutton.png"));
        starButtonTexRegion = new TextureRegion(startButtonTexture);
        startButtonTexRegionDrawable = new TextureRegionDrawable(starButtonTexRegion);
        startButton = new ImageButton(startButtonTexRegionDrawable);

        exitButtonTexture = new Texture(Gdx.files.internal("exitbutton.png"));
        exitButtonTexRegion = new TextureRegion(exitButtonTexture);
        exitButtonTexRegionDrawable = new TextureRegionDrawable(exitButtonTexRegion);
        exitButton = new ImageButton(exitButtonTexRegionDrawable);

        loadButtonTexture = new Texture(Gdx.files.internal("loadbutton.png"));
        loadButtonTexRegion = new TextureRegion(loadButtonTexture);
        loadButtonTexRegionDrawable = new TextureRegionDrawable(loadButtonTexRegion);
        loadButton = new ImageButton(loadButtonTexRegionDrawable);

        /**
         * Create listener for mouse click
         */
        startButton.addListener(new InputListener()
        {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                startGame(false);
                return false;
            }
        });

        exitButton.addListener(new InputListener()
        {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                exitGame();
                return false;
            }
        });

        loadButton.addListener(new InputListener()
        {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button){
                startGame(true);
                return false;
            }

        });

        /**
         * Create table to organize buttons position
         */
        Table table = new Table();
        table.setFillParent(true);
        table.add(startButton).padBottom(20);
        table.row();
        table.add(loadButton).padBottom(20);
        table.row();
        table.add(exitButton);
        stage.addActor(table);
    }

    /**
     * Menu loop
     * @param delta time
     */
    @Override
    public void render(float delta) {
        /**
         * Render stage
         */
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    /**
     * Free memory
     */
    @Override
    public void dispose() {
        stage.dispose();
        batch.dispose();
        texture.dispose();
        startButtonTexture.dispose();
        exitButtonTexture.dispose();
    }

    public void startGame(boolean load){
        game.setScreen(new GameScreen(game, load));
    }

    public void exitGame(){
        Gdx.app.exit();
    }

}
