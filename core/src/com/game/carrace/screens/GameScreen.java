package com.game.carrace.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.*;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.shaders.DefaultShader;
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController;
import com.badlogic.gdx.graphics.g3d.utils.DefaultShaderProvider;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.Bullet;
import com.badlogic.gdx.physics.bullet.collision.*;
import com.badlogic.gdx.utils.Array;
import com.game.carrace.CarRace;
import com.game.carrace.GameScreenInputProcessor;
import com.game.carrace.GameState;
import java.io.*;

/**
 * GameScreen.java - class for displaying and calculating game scene
 * @author Mateusz Kud&#x142;a
 */

public class GameScreen implements Screen {
    /**
     * Main application object
     */
    private CarRace game;
    /**
     * Main camera
     */
    private PerspectiveCamera cam;
    /**
     * Holds model instances for rendering
     */
    private ModelBatch modelBatch;
    /**
     * Contains
     */
    private Environment environment;
    /**
     *
     */
    private AssetManager assetManager;
    private GameScreenInputProcessor inputProcessor= new GameScreenInputProcessor(this);

    private Array<ModelInstance> instances = new Array<ModelInstance>();
    private ModelInstance track;
    private ModelInstance car;
    private Array<ModelInstance> checkpoints = new Array<ModelInstance>();
    private Array<btCollisionObject> checkpointsCollisionObject = new Array<btCollisionObject>();
    private btCollisionShape checkpointsCollisionShape;
    private Model checkpoint;

    private btCollisionShape trackShape;
    private btCollisionShape carShape;

    private btCollisionObject trackObject;
    private btCollisionObject carObject;
    private CameraInputController camController;
    private btCollisionConfiguration collisionConfig;
    private btDispatcher dispatcher;

    private Vector3 carPosition;
    private Vector3 camPosition;

    /**
     * True if models are loading
     */
    private boolean loading = true;
    private boolean collision;

    private boolean forwardMove = false;
    private boolean backwardMove = false;
    private boolean leftMove = false;
    private boolean rightMove = false;

    /**
     * Cam distance from car
     */
    private float camOffsetY = 1f;
    private float camOffsetX = 0f;
    private float camOffsetZ = 5f;

    /**
     * If true, player can move camera by mouse
     * If false, camera follows the car
     */
    private boolean freeCam = false;

    /**
     * If false, player cannot control the car
     */
    private boolean canDrive = false;

    private float timeCounter = 0;

    private int checkpointCounter = 0;

    /**
     * Holds state of all checkpoints
     * True: player moved through given checkpoint
     */
    private boolean[] checkpointState  = {false, false, false, false, false};

    private Shader shader;

    /**
     * Holds sprites
     */
    private SpriteBatch batch;

    private BitmapFont checkPointText = new BitmapFont();
    private BitmapFont timerText = new BitmapFont();
    Sound sound;

    private boolean loadGame;

    private Texture checkpointTexture;

    /**
     *
     * @param game main application object
     * @param load true: deserializes game state,
     *             false: move game objects to starting positions
     */
    public GameScreen(CarRace game, boolean load) {
        this.loadGame = load;
        this.game = game;
        batch = new SpriteBatch();
        Gdx.input.setInputProcessor(inputProcessor);
    }

    /**
     * Executes once after object creation
     */
    @Override
    public void show() {
        /**
         * Enable BulletPhysics for collisions
         */
        Bullet.init();
        /**
         * Light setup
         */
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, 0f, -0.8f, 0f));

        DefaultShader.Config config = new DefaultShader.Config();
        config.numDirectionalLights = 1;
        config.numPointLights = 0;
        config.numBones = 16;
        modelBatch = new ModelBatch(new DefaultShaderProvider());
        //shader = new DefaultShader();

        /**
         * Setup camera and it's starting position
         */
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        //if(!loadGame)
            cam.position.set(25f, 0f, 100f);
        cam.lookAt(25, 0, 0);
        cam.near = 1f;
        cam.far = 500f;
        cam.update();

        /**
         * Load models from files
         */
        assetManager = new AssetManager();
        assetManager.load("car.g3db", Model.class);
        assetManager.load("track.g3db", Model.class);
        loading = true;

        /**
         * Model shapes used in collision
         */
        trackShape = new btBoxShape(new Vector3(100, 5.3f, 100f));
        carShape = new btBoxShape(new Vector3(2.5f, 0.5f, 2.5f));

        /**
         * Model objects - contain their shape's position
         */
        trackObject = new btCollisionObject();
        trackObject.setCollisionShape(trackShape);
        carObject = new btCollisionObject();
        carObject.setCollisionShape(carShape);

        /**
         * Create box models used as checkpoints
         */
        ModelBuilder modelBuilder = new ModelBuilder();
        for(int i=0; i<5; i++) {
            checkpoint = modelBuilder.createBox(5f, 5f, 0.1f,
                    new Material(ColorAttribute.createDiffuse(Color.GREEN)),
                    VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
            checkpoints.add(new ModelInstance(checkpoint));
        }
        checkpointsCollisionShape = new btBoxShape(new Vector3(5f,5f, 0.1f));

        for(int i=0; i<5; i++){
            checkpointsCollisionObject.add(new btCollisionObject());
            checkpointsCollisionObject.get(i).setCollisionShape(checkpointsCollisionShape);
        }


        camController = new CameraInputController(cam);
        Gdx.input.setInputProcessor((freeCam)?camController:inputProcessor);

        /**
         * Handle collision between two objects
         */
        collisionConfig = new btDefaultCollisionConfiguration();
        dispatcher = new btCollisionDispatcher(collisionConfig);

        checkpointTexture = new Texture(Gdx.files.internal("checkpoint.png"));
        sound = Gdx.audio.newSound(Gdx.files.internal("bg.mp3"));
    }

    /**
     * Game loop, everything is rendering here
     * @param delta time
     */
    @Override
    public void render(float delta) {
        /**
         * Update the timer
         */
        if(canDrive)
            timeCounter+=delta;
        final float mDelta = Math.min(1f/30f, Gdx.graphics.getDeltaTime());

        /**
         * Wait until all models are loaded
         */
        if (loading && assetManager.update())
            doneLoading();
        /**
         * Clear screen
         */
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

        /**
         * Render all model instances and environment
         */
        modelBatch.begin(cam);
        modelBatch.render(instances, environment);
        modelBatch.end();

        /**
         * Makes the car fall at the beginning
         */
        if (!collision && car!=null) {
            car.transform.translate(0f, -mDelta, 0f);
            carObject.setWorldTransform(car.transform);
            collision = checkCollision(carObject, trackObject);
            canDrive = false;
        }else if(!collision){

        }else{
            canDrive = true;
        }

        /**
         * Moves the car if it's not null
         */
        if(car!=null){
            moveCar();
        }

        /**
         * Renders all texts
         */
        batch.begin();
        checkPointText.draw(batch, "Checkpoints: "+checkpointCounter+"/5", 256, 450);
        timerText.draw(batch, "Time: " + String.format("%.2f", timeCounter), 275, 425);
        batch.end();

        /**
         * Checks if car move through the checkpoint
         */
        if(canDrive)
            checkChekcpoint();


        /**
         * Check if car moved through all checkpoints
         */
        if(checkpointCounter >= 5){
            canDrive = false;
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    /**
     * Frees the memory at application exit
     */
    @Override
    public void dispose() {
        modelBatch.dispose();
        carObject.dispose();
        carShape.dispose();
        trackShape.dispose();
        trackObject.dispose();
        collisionConfig.dispose();
        dispatcher.dispose();
        assetManager.dispose();
        shader.dispose();
        checkpoint.dispose();
        for(btCollisionObject o : checkpointsCollisionObject)
            o.dispose();
        checkpointsCollisionShape.dispose();
    }

    /**
     * Initializes objects and their position when they are loaded
     */
    private void doneLoading() {
        /**
         * Initialize car and track objects
         */
        track = new ModelInstance(assetManager.get("track.g3db", Model.class));
        car = new ModelInstance(assetManager.get("car.g3db", Model.class));


        if(loadGame){
            /**
             * Deserialize GameState object and set model position
             */
            GameState gameState = null;
            try {
                FileInputStream fileIn = new FileInputStream("gamestate.ser");
                ObjectInputStream in = new ObjectInputStream(fileIn);
                gameState = (GameState) in.readObject();
                in.close();
                fileIn.close();
            } catch (IOException i) {
                i.printStackTrace();
                return;
            } catch (ClassNotFoundException c) {
                System.out.println("GameState class not found");
                c.printStackTrace();
                return;
            }

            checkpointState = gameState.getCheckpointState();
            car.transform.setTranslation(gameState.getCarPosition());
            car.transform.rotate(gameState.getCarRotation());
            cam.position.set(gameState.getCamPosition());
            carPosition = car.transform.getTranslation(new Vector3());
            camPosition = new Vector3(carPosition.x, carPosition.y+1, carPosition.z+5);
            camPosition.rotate(-18f, 0f, 1f, 0f);
            cam.position.set(camPosition);

            checkpointCounter = gameState.getCheckpointCounter();
            timeCounter = gameState.getTimeCounter();
            loadGame = false;
        }else{
            /**
             * Set objects starting position
             */
            for(int i=0; i<5; i++){
                checkpointState[i]=false;
            }
            car.transform.translate(26f, 8f, -2f);
            car.transform.rotate(0, 1, 0, -15);
            carPosition = car.transform.getTranslation(new Vector3());
            camPosition = new Vector3(carPosition.x, carPosition.y, carPosition.z+5);
            camPosition.rotate(-18f, 0f, 1f, 0f);
            cam.position.set(camPosition);
    }

        car.transform.scale(0.5f, 0.5f, 0.5f);
        car.materials.get(0).set(new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA));
        cam.update();

        /**
         * Set checkpoints position
         */
        checkpoints.get(0).transform.translate(8, 5, -36.5f);
        checkpoints.get(0).transform.rotate(0, 1, 0, 90);
        checkpoints.get(1).transform.translate(-30, 5, -15);
        checkpoints.get(1).transform.rotate(0, 1, 0, -25);
        checkpoints.get(2).transform.translate(-6, 5, 7);
        checkpoints.get(2).transform.rotate(0,1,0, -88);
        checkpoints.get(3).transform.translate(22, 5, 37);
        checkpoints.get(3).transform.rotate(0, 1, 0, 95);
        checkpoints.get(4).transform.translate(24, 5, 6);
        checkpoints.get(4).transform.rotate(0, 1, 0, -14);

        /**
         * Add model instances to Array
         */
        instances.add(track);
        instances.add(car);
        instances.add(checkpoints.get(0));
        instances.add(checkpoints.get(1));
        instances.add(checkpoints.get(2));
        instances.add(checkpoints.get(3));
        instances.add(checkpoints.get(4));

        /**
         * Set ModelObjects position to ModelInstances position
         */
        checkpoints.get(0).nodes.get(0).parts.get(0).material.set(new Material(TextureAttribute.createDiffuse(checkpointTexture)));

        carObject.setWorldTransform(car.transform);
        trackObject.setWorldTransform(track.transform);
        checkpointsCollisionObject.get(0).setWorldTransform(checkpoints.get(0).transform);
        checkpointsCollisionObject.get(1).setWorldTransform(checkpoints.get(1).transform);
        checkpointsCollisionObject.get(2).setWorldTransform(checkpoints.get(2).transform);
        checkpointsCollisionObject.get(3).setWorldTransform(checkpoints.get(3).transform);
        checkpointsCollisionObject.get(4).setWorldTransform(checkpoints.get(4).transform);

        sound.play(1.0f);
        loading = false;
    }

    /**
     * Checks collision between two objects
     * @param obj0 first object
     * @param obj1 second object
     * @return true: collision, false: no collision
     */
    boolean checkCollision(btCollisionObject obj0, btCollisionObject obj1) {
        CollisionObjectWrapper co0 = new CollisionObjectWrapper(obj0);
        CollisionObjectWrapper co1 = new CollisionObjectWrapper(obj1);

        /**
         * Choose alghoritm for collisions
         */
        btCollisionAlgorithmConstructionInfo ci = new btCollisionAlgorithmConstructionInfo();
        ci.setDispatcher1(dispatcher);
        btCollisionAlgorithm algorithm = new btSphereBoxCollisionAlgorithm(null, ci, co0.wrapper, co1.wrapper, false);

        btDispatcherInfo info = new btDispatcherInfo();
        btManifoldResult result = new btManifoldResult(co0.wrapper, co1.wrapper);

        /**
         * Check collision
         */
        algorithm.processCollision(co0.wrapper, co1.wrapper, info, result);

        boolean r = result.getPersistentManifold().getNumContacts() > 0;

        result.dispose();
        info.dispose();
        algorithm.dispose();
        ci.dispose();
        co1.dispose();
        co0.dispose();

        return r;
    }

    /**
     * Checks if there is collision between checkpoint and player,
     * changes state of checkpoint if there is collision to not count
     * one checkpoint multiple times
     */
    void checkChekcpoint(){
        for(int i=0; i<5; i++) {
            if (!checkpointState[i]) {
                if(checkCollision(carObject, checkpointsCollisionObject.get(i))){
                    checkpointCounter++;
                    checkpointState[i] = true;
                }
            }
        }
    }

    /**
     * Moves the car and rotates the camera
     */
    public void moveCar() {
        carPosition = car.transform.getTranslation(new Vector3());
        camPosition = new Vector3(carPosition.x + camOffsetX, carPosition.y + camOffsetY, carPosition.z + camOffsetZ);
        if(!freeCam) {
            cam.lookAt(carPosition);
            moveCam(0.09f);
        }
        if(canDrive) {
            if (forwardMove) {
                car.transform.translate(0, 0, -0.15f);
            }
            if (backwardMove) {
                car.transform.translate(0, 0, 0.1f);
            }
            if (leftMove) {
                if (forwardMove) {
                    car.transform.rotate(0, 1, 0, 0.6f);
                    cam.rotateAround(carPosition, new Vector3(0, 1, 0), 0.6f);
                }
                if (backwardMove) {
                    car.transform.rotate(0, 1, 0, -0.5f);
                    cam.rotate(0.5f, 0f, 1f, 0f);
                }
            }
            if (rightMove) {
                if (forwardMove) {
                    car.transform.rotate(0, 1, 0, -0.6f);
                    cam.rotateAround(carPosition, new Vector3(0, 1, 0), -0.6f);
                }
                if (backwardMove) {
                    car.transform.rotate(0, 1, 0, 0.5f);
                }
            }
        }
    }

    /**
     * Moves the camera to the direction it's looking into
     * @param a moving speed
     */
    void moveCam(float a) {
        if(cam.position.dst(carPosition) > 4){
            cam.position.add(cam.direction.x * a, 0, cam.direction.z * a);

        }
        carObject.setWorldTransform(car.transform);
        cam.update();
    }

    /**
     * Enables/Disables moving forward
     * @param t true: move, false: not move
     */
    public void setForwardMove(boolean t){
        if(backwardMove && t)
            backwardMove = false;
        forwardMove = t;
    }

    /**
     * Enables/Disables moving backward
     * @param t true: move, false: not move
     */
    public void setBackwardMove(boolean t){
        if(forwardMove && t)
            forwardMove = false;
        backwardMove = t;
    }

    /**
     * Enables/Disables moving left
     * @param t true: move, false: not move
     */
    public void setLeftMove(boolean t)
    {
        if(rightMove && t)
            rightMove = false;
        leftMove = t;
    }

    /**
     * Enables/Disables moving right
     * @param t true: move, false: not move
     */
    public void setRightMove(boolean t)
    {
        if(leftMove && t)
            leftMove = false;
        rightMove = t;
    }

    /**
     * Sets models position and rotation to GameState and serializes the object
     */
    public void saveGameState(){
        GameState gameState = new GameState();
        gameState.setCarPosition(carPosition);
        gameState.setCarRotation(car.transform.getRotation(new Quaternion()));
        gameState.setCheckpointCounter(checkpointCounter);
        gameState.setCheckpointState(checkpointState);
        gameState.setTimeCounter(timeCounter);
        gameState.setCamPosition(cam.position);

        try {
            FileOutputStream fileOut =
                    new FileOutputStream("gamestate.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(gameState);
            out.close();
            fileOut.close();
            System.out.printf("Serialized");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }
}
